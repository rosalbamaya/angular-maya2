import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs'; 
import {Solicitud} from './modelos/solicitud.model';


@Injectable()
export class SolicitudService {
  private readonly baseURL: string;
 
  private httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };

  constructor(private http: HttpClient) { 
   this.baseURL = 'http://192.168.11.1:8091/servicios/';
   console.log('funcionando servicio')
  }
  

 getSolicitudes(): Observable<Solicitud[]> {
    const headers = new HttpHeaders();
    headers.append('Content-Type', 'application/json; charset=utf-8');
    return this.http.get<Solicitud[]>(this.baseURL + 'getALLSolicitudes');
  }
}
